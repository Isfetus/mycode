//
//  Constants.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

struct Constants {
    static let mainStoryoardName = "Main"
    static let chartComponentControllerStoryboardId = "ChartComponentController" 
    static let mainControllerStoryoardId = "MainViewController" 
}
