//
//  BondValuePlain.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 16/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit

struct BondValuePlain {
    var price: Double
    var yield: Double
    var date: Date
}
