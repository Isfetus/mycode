//
//  Double+Random.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 18/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import Foundation

extension Double {

    mutating func round(minLimit: Double, maxLimit: Double) -> Double {
        if self >= maxLimit {
            return maxLimit
        } else if self < minLimit {
            return minLimit
        }
        return self
    }

    static func randomValueBetweenMinusOneToOne() -> Double {
        let randomInt = Int(arc4random_uniform(3)) - 1
        let randomDouble : Double = Double(randomInt)
        return randomDouble
    }
}



