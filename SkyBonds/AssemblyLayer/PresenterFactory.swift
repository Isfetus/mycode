//
//  PresenterFactory.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit

class PresenterFactory: NSObject {
    static func chartComponentPresenter(view: ChartComponentController, bondType: String) -> ChartComponentPresenter {
        let presenter = ChartComponentPresenter(view: view, service: ServiceFactory.stubBondDataService())
        presenter.bondType = bondType
        return presenter
    }
}
