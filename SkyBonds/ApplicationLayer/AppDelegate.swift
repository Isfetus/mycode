//
//  AppDelegate.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 14/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow()
        self.window?.rootViewController = ViewFactory.mainController()
        self.window?.makeKeyAndVisible()

        return true
    }
}

