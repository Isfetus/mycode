//
//  ChartComponentViewOutput.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit

protocol ChartComponentViewOutput {
    func didChangeState(state: ChartComponentState)
    func didChangeBondType(bondType: String)
}
